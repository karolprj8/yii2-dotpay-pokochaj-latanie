DotPay extension for the Yii2
===========

DotPay payment extension for the Yii2.

Installation
====

Add to the composer.json file following section:

```
php composer.phar require --prefer-dist karolprj8/yii2-dotpay "*"
```

```
"karolprj8/yii2-dotpay": "dev-master"
```

Add to to you Yii2 config file this part with component settings:

```php
'dotpay'=> [
    'class'        => 'karolprj8\DotPay',
    'clientId'     => 'you_client_id',
    'clientSecret' => 'you_client_secret',
    'isProduction' => false,
     // This is config file for the DotPay system
     'config'       => [
         'http.ConnectionTimeOut' => 30,
         'http.Retry'             => 1
    ]
],
```
